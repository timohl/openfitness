[![tests](https://github.com/timohl/openfitness/actions/workflows/tests.yml/badge.svg)](https://github.com/timohl/openfitness/actions/workflows/tests.yml)

# openfitness
Fitness app for creating customized exercises and workouts.
