import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'package:openfitness/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  final binding = IntegrationTestWidgetsFlutterBinding.ensureInitialized()
    as IntegrationTestWidgetsFlutterBinding;
  testWidgets('Screenshot home', (tester) async {
    await (await SharedPreferences.getInstance()).clear();
    SharedPreferences.setMockInitialValues({'pref_show_intro': false});
    initAndRun(const OpenfitnessApp(initialUrl: '/home'));
    await tester.pumpAndSettle();
    await binding.convertFlutterSurfaceToImage();
    await tester.pump();
    await binding.takeScreenshot('home');
  });
  testWidgets('Screenshot settings', (tester) async {
    await (await SharedPreferences.getInstance()).clear();
    SharedPreferences.setMockInitialValues({'pref_show_intro': false});
    initAndRun(const OpenfitnessApp(initialUrl: '/settings'));
    await tester.pumpAndSettle();
    await binding.convertFlutterSurfaceToImage();
    await tester.pump();
    await binding.takeScreenshot('settings');
  });
}