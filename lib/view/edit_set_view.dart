import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:openfitness/model/gated_set_element.dart';
import 'package:provider/provider.dart';
import 'package:vrouter/vrouter.dart';
import 'package:flutter_switch/flutter_switch.dart';

import 'package:openfitness/model/workouts_model.dart';
import 'package:openfitness/model/set_types_model.dart';
import 'package:openfitness/model/execution_types_model.dart';
import 'package:openfitness/model/exercises_model.dart';
import 'package:openfitness/widget/navigation_drawer.dart';

class EditSetView extends StatefulWidget {
  const EditSetView({Key? key}) : super(key: key);

  @override
  EditSetViewState createState() => EditSetViewState();
}

class EditSetViewState extends State<EditSetView> {
  final _formKey = GlobalKey<FormState>();
  int? _typeID;
  int? _executionTypeID;
  int? _exerciseIndex;
  bool? _groupWithPrevious;
  int? _minReps;
  int? _maxReps;
  int? _hold;
  int? _lenght;
  int? _rest;
  int? _repRest;

  @override
  Widget build(BuildContext context) {
    final workoutIndexStr = context.vRouter.pathParameters['workoutIndex'];
    final workoutIndex = workoutIndexStr != null? int.parse(workoutIndexStr): null;
    final setIndexStr = context.vRouter.pathParameters['setIndex'];
    final setIndex = setIndexStr != null? int.parse(setIndexStr): null;
    // TODO: workoutIndex null check
    return Consumer3<SetTypesModel, ExecutionTypesModel, WorkoutsModel>(
      builder: (context, setTypesModel, executionTypesModel, workoutsModel, _) {
        final setElement = setIndex != null && workoutIndex != null ? workoutsModel.list[workoutIndex].setElements[setIndex]: null;
        _typeID ??= setElement?.typeID;
        _executionTypeID ??= setElement?.executionTypeID;
        final gatedSetElement = GatedSetElement(
          setType: setTypesModel.setTypeFromID(_typeID),
          executionType: executionTypesModel.executionTypeFromId(_executionTypeID),
        );
        return Form(
          key: _formKey,
          child: Scaffold(
            drawer: const NavigationDrawer(),
            appBar: AppBar(
              title: Text(
                setElement != null? 'Edit Set': 'Add Set',
              ),
            ),
            body: Column(
              children: <Widget>[
                _buildSetType(context),
                _buildExecutionType(context),
                _buildGroupWithPrevious(context, gatedSetElement),
                _buildReps(context, gatedSetElement),
                _buildHold(context, gatedSetElement),
                _buildLenght(context, gatedSetElement),
                _buildRest(context, gatedSetElement),
                _buildRepRest(context, gatedSetElement),
                Expanded(child: _buildExercisesList(context, setElement),),
              ],
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () async {
                if(_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  int? exerciseID = _exerciseIndex != null? Provider.of<ExercisesModel>(context, listen: false).list[_exerciseIndex!].id: null;
                  await EasyLoading.show(status: 'Saving changes...');
                  final setElementData = SetElementData(
                    exerciseID: exerciseID,
                    typeID: _typeID!,
                    executionTypeID: _executionTypeID!,
                    groupWithPrevious: _groupWithPrevious,
                    minReps: _minReps,
                    maxReps: _maxReps,
                    hold: _hold,
                    lenght: _lenght,
                    rest: _rest,
                    repRest: _repRest,
                  );
                  if(setIndex != null) {
                    await workoutsModel.updateSet(
                      setElement: setElementData,
                      listIndex: setIndex,
                      workoutListIndex: workoutIndex!,
                    );
                  } else {
                    await workoutsModel.addSet(
                      setElement: setElementData,
                      workoutListIndex: workoutIndex!,
                    );
                  }
                  await EasyLoading.showSuccess('Finished');
                  context.vRouter.pop();
                }
              },
              tooltip: 'Done',
              child: const Icon(Icons.done),
            ),
          ),
        );
      },
    );
  }
  
  Widget _buildSetType(BuildContext context) {
    return Consumer<SetTypesModel>(
      builder: (context, SetTypesModel model, child) {
        return DropdownButtonFormField<int>(
          decoration: const InputDecoration(
            labelText: 'Type',
          ),
          value: _typeID,
          items: model.list.map((SetTypeData setType) {
            return DropdownMenuItem<int>(
              value: setType.id,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(setType.title),
                  if(setType.description != null) Text("\t" + setType.description!, textScaleFactor: .8,),
                ],
              ),
            );
          }).toList(),
          selectedItemBuilder: (BuildContext context) {
            return model.list.map<Widget>((SetTypeData setType) {
              return Text(setType.title);
            }).toList();
          },
          validator: (value) => value == null? 'Please enter a set type.': null,
          onChanged: (value) {
            setState((){
              _typeID = value;
            });
          },
          onSaved: (value){_typeID = value;},
        );
      },
    );
  }

  Widget _buildExecutionType(BuildContext context) {
    return Consumer<ExecutionTypesModel>(
      builder: (context, ExecutionTypesModel workouts, child) {
        return DropdownButtonFormField<int>(
          decoration: const InputDecoration(
            labelText: 'Execution Type',
          ),
          value: _executionTypeID,
          items: workouts.list.map((ExecutionTypeData executionType) {
            return DropdownMenuItem<int>(
              value: executionType.id,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(executionType.title),
                  if(executionType.description != null)
                    Text(executionType.description!, textScaleFactor: .8,),
                ],
              ),
            );
          }).toList(),
          selectedItemBuilder: (BuildContext context) {
            return workouts.list.map<Widget>((ExecutionTypeData executionType) {
              return Text(executionType.title);
            }).toList();
          },
          validator: (value) => value == null? 'Please select execution type.': null,
          onChanged: (value){
            setState(() {
              _executionTypeID = value;
            });
          },
          onSaved: (value){_executionTypeID = value;},
        );
      },
    );
  }

  Widget _buildGroupWithPrevious(
    BuildContext context,
    GatedSetElement gatedSetElement,
  ) => Visibility(
    visible: gatedSetElement.groupWithPrevious.enabled,
    maintainState: true,
    child: Row(
      children: [
        const Text('Group with previous:'),
        Padding(
          padding: const EdgeInsets.all(10),
          child: FormField(
            initialValue: gatedSetElement.groupWithPrevious.value ?? false,
            onSaved: (bool? value) {_groupWithPrevious = value;},
            builder: (FormFieldState<bool> state) => FlutterSwitch(
              value: state.value!,
              onToggle: (bool value) {
                setState(() {
                  state.didChange(value);
                });
              },
            ),
          ),
        ),
      ],
    ),
  );
  
  // TODO: Add checkbox to just select single reps value
  Widget _buildReps(
    BuildContext context,
    GatedSetElement gatedSetElement,
  ) => Visibility(
    visible: gatedSetElement.minReps.enabled && gatedSetElement.maxReps.enabled,
    maintainState: true,
    child: Row(
      children: [
        const Text('Reps:'),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(5),
            child: FormField(
              initialValue: RangeValues(
                (gatedSetElement.minReps.value ?? 0).toDouble(),
                (gatedSetElement.maxReps.value ?? 12).toDouble(),
              ),
              onSaved: (RangeValues? value) {
                _minReps = value?.start.round();
                _maxReps = value?.end.round();
              },
              builder: (FormFieldState<RangeValues> state) => Row(
                children: [
                  Text(state.value?.start.toStringAsFixed(0) ?? ''),
                  Expanded(
                    child: RangeSlider(
                      min: 0,
                      max: 24,
                      divisions: 24,
                      values: state.value!,
                      onChanged: (RangeValues value) {
                        setState(() {
                          state.didChange(value);
                        });
                      },
                    ),
                  ),
                  Text(state.value?.end.toStringAsFixed(0) ?? ''),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
  );
  
  Widget _buildPositiveIntegerFormField({
    required String initialValue,
    required void Function(String?) onSaved,
  }) => TextFormField(
    initialValue: initialValue,
    onSaved: onSaved,
    keyboardType: const TextInputType.numberWithOptions(),
    inputFormatters: [
      FilteringTextInputFormatter.digitsOnly,
    ],
    validator: (String? value) {
      if(value == null || value == '') {
        return null;
      } else if(value.startsWith('0')) {
        return 'Please do not start numbers with 0';
      } else if(int.tryParse(value) != null) {
        return null;
      } else {
        return 'Enter valid positive integer.';
      }
    },
  autovalidateMode: AutovalidateMode.onUserInteraction,
  );
  
  int? _parseIntIfNotEmpty(String? value) => value != null && value != ''? int.parse(value): null;

  Widget _buildHold(
    BuildContext context,
    GatedSetElement gatedSetElement,
  ) => Visibility(
    visible: gatedSetElement.hold.enabled,
    maintainState: true,
    child: Row(
      children: [
        const Text('Hold:'),
        Expanded(
          child: _buildPositiveIntegerFormField(
            initialValue: gatedSetElement.hold.value?.toString() ?? '',
            onSaved: (String? value) {
              _hold = _parseIntIfNotEmpty(value);
            },
          ),
        ),
      ],
    ),
  );
  
  Widget _buildLenght(
    BuildContext context,
    GatedSetElement gatedSetElement,
  ) => Visibility(
    visible: gatedSetElement.lenght.enabled,
    maintainState: true,
    child: Row(
      children: [
        const Text('Lenght:'),
        Expanded(
          child: _buildPositiveIntegerFormField(
            initialValue: gatedSetElement.lenght.value?.toString() ?? '',
            onSaved: (String? value) {
              _lenght = _parseIntIfNotEmpty(value);
            },
          ),
        ),
      ],
    ),
  );
  
  Widget _buildRest(
    BuildContext context,
    GatedSetElement gatedSetElement,
  ) => Visibility(
    visible: gatedSetElement.rest.enabled,
    maintainState: true,
    child: Row(
      children: [
        const Text('Rest:'),
        Expanded(
          child: _buildPositiveIntegerFormField(
            initialValue: gatedSetElement.rest.value?.toString() ?? '',
            onSaved: (String? value) {
              _rest = _parseIntIfNotEmpty(value);
            },
          ),
        ),
      ],
    ),
  );

  Widget _buildRepRest(
    BuildContext context,
    GatedSetElement gatedSetElement,
  ) => Visibility(
    visible: gatedSetElement.repRest.enabled,
    maintainState: true,
    child: Row(
      children: [
        const Text('RepRest:'),
        Expanded(
          child: _buildPositiveIntegerFormField(
            initialValue: gatedSetElement.repRest.value?.toString() ?? '',
            onSaved: (String? value) {
              _repRest = _parseIntIfNotEmpty(value);
            },
          ),
        ),
      ],
    ),
  );

  Widget _buildExercisesList(BuildContext context, SetElementData? setElement) {
    return FormField(
      initialValue: setElement?.exerciseID != null?
        Provider.of<ExercisesModel>(context, listen: false).listIndexFromID(setElement!.exerciseID):
        null,
      onSaved: (int? exerciseIndex) {_exerciseIndex = exerciseIndex;},
      builder: (FormFieldState<int> state) => Consumer<ExercisesModel>(
        builder: (context, ExercisesModel exercises, child) {
          return ListView.builder(
            padding: const EdgeInsets.all(16.0),
            itemCount: exercises.list.length * 2,
            itemBuilder: (context, i) {
              if(i.isOdd) return const Divider();
              int index = (i ~/ 2);
              return _buildExerciseTile(context, index, exercises.list[index], state);
            },
          );
        },
      ),
    );
  }

  Widget _buildExerciseTile(BuildContext context, int index, ExerciseData exercise, FormFieldState<int> state) {
    return ListTile(
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(exercise.title),
          Text(exercise.description ?? "No description", textScaleFactor: .8,),
        ],
      ),
      selected: state.value != null? index == state.value: false,
      onTap: () {
        setState(() {state.didChange(index);});
      },
    );
  }
}