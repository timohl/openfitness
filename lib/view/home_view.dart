import 'package:flutter/material.dart';

import 'package:openfitness/widget/navigation_drawer.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavigationDrawer(),
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: const Text('Planned workouts for today:'),
    );
  }
}