import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:vrouter/vrouter.dart';

import 'package:openfitness/model/workouts_model.dart';
import 'package:openfitness/widget/navigation_drawer.dart';
import 'package:openfitness/widget/listview_with_divider.dart';
import 'package:openfitness/widget/slidable_tile.dart';

class WorkoutsView extends StatelessWidget {
  const WorkoutsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavigationDrawer(),
      appBar: AppBar(
        title: const Text('Workouts'),
      ),
      body: Consumer<WorkoutsModel>(
        builder: (context, WorkoutsModel workoutsModel, child) => ListViewWithDivider(
          itemCount: workoutsModel.list.length,
          tileBuilder: (context, index) => SlidableTile(
            title: Text(workoutsModel.list[index].title),
            onStart: (context) {
              context.vRouter.to('execute/$index');
            },
            onEdit: (context) {
              context.vRouter.to('edit/$index');
            },
            onTap: () {
              context.vRouter.to('sets/$index');
            },
            onDelete: (context) async {
              await EasyLoading.show(status: 'Deleting workout...');
              await Provider.of<WorkoutsModel>(context, listen: false).delete(index);
              await EasyLoading.showSuccess('Finished');
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {context.vRouter.to('edit');},
        tooltip: 'Add a new workout.',
        child: const Icon(Icons.add),
      ),
    );
  }
}