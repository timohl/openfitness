import 'package:flutter/material.dart';

import 'package:dynamic_themes/dynamic_themes.dart';
import 'package:pref/pref.dart';

import 'package:openfitness/widget/navigation_drawer.dart';

class AppThemes {
  static const int dark = 0;
  static const int light = 1;
  static final collection = ThemeCollection(
    themes: {
      AppThemes.dark: ThemeData.dark(),
      AppThemes.light: ThemeData.light(),
    },
    fallbackTheme: ThemeData.fallback(),
  );
}

class SettingsView extends StatelessWidget {
  const SettingsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavigationDrawer(),
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: PrefPage(children: [
        const PrefTitle(title: Text('Theme')),
        PrefRadio(
          title: const Text('Dark Theme'),
          value: 'dark',
          pref: 'ui_theme',
          onSelect: () {
            DynamicTheme.of(context)?.setTheme(AppThemes.dark);
          },
        ),
        PrefRadio(
          title: const Text('Light Theme'),
          value: 'light',
          pref: 'ui_theme',
          onSelect: () {
            DynamicTheme.of(context)?.setTheme(AppThemes.light);
          },
        ),
        const PrefTitle(title: Text('Intro')),
        const PrefSwitch(
          title: Text('Show intro again'),
          pref: 'show_intro',
        ),
      ]),
    );
  }
}
