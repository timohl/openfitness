import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:vrouter/vrouter.dart';

import 'package:openfitness/model/workouts_model.dart';
import 'package:openfitness/widget/navigation_drawer.dart';

class EditWorkoutView extends StatefulWidget {
  const EditWorkoutView({Key? key}) : super(key: key);

  @override
  EditWorkoutViewState createState() => EditWorkoutViewState();
}

class EditWorkoutViewState extends State<EditWorkoutView> {
  final _formKey = GlobalKey<FormState>();
  String? _name;
  String? _description;

  @override
  Widget build(BuildContext context) {
    final workoutIndexStr = context.vRouter.pathParameters['workoutIndex'];
    final workoutIndex = workoutIndexStr != null? int.parse(workoutIndexStr): null;
    // TODO: Consumer or Provider.of?
    final workout = workoutIndex != null? Provider.of<WorkoutsModel>(context, listen: false).list[workoutIndex]: null;
    return Form(
      key: _formKey,
      child: Scaffold(
        drawer: const NavigationDrawer(),
        appBar: AppBar(
          title: Text(
            workout != null? 'Edit Workout': 'Add Workout',
          ),
        ),
        body: Column(
          children: <Widget>[
            TextFormField(
              decoration: const InputDecoration(
                labelText: 'Name',
              ),
              validator: (value) => value?.isEmpty ?? false? 'Please enter a name.': null,
              onSaved: (value){_name = value;},
              initialValue: workout?.title ?? "",
            ),
            TextFormField(
              decoration: const InputDecoration(
                labelText: 'Description',
              ),
              onSaved: (value){_description = value != ''? value: null;},
              initialValue: workout?.description ?? "",
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            if(_formKey.currentState!.validate()) {
              _formKey.currentState!.save();
              await EasyLoading.show(status: 'Saving changes...');
              final workoutData = WorkoutData(
                title: _name!,
                description: _description,
              );
              if(workoutIndex != null) {
                await Provider.of<WorkoutsModel>(context, listen: false).update(workoutData, workoutIndex);
              } else {
                await Provider.of<WorkoutsModel>(context, listen: false).add(workoutData);
              }
              await EasyLoading.showSuccess('Finished');
              context.vRouter.pop();
            }
          },
          tooltip: 'Done',
          child: const Icon(Icons.done),
        ),
      ),
    );
  }
}