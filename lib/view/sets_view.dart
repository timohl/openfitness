
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:openfitness/widget/listview_with_divider.dart';
import 'package:openfitness/widget/slidable_tile.dart';
import 'package:provider/provider.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:vrouter/vrouter.dart';

import 'package:openfitness/model/workouts_model.dart';
import 'package:openfitness/model/set_types_model.dart';
import 'package:openfitness/model/exercises_model.dart';
import 'package:openfitness/widget/navigation_drawer.dart';

class SetsView extends StatelessWidget {
  const SetsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final workoutIndexStr = context.vRouter.pathParameters['workoutIndex'];
    final workoutIndex = workoutIndexStr != null? int.parse(workoutIndexStr): null;
    // TODO: Consumer or Provider.of?
    final workout = workoutIndex != null? Provider.of<WorkoutsModel>(context, listen: false).list[workoutIndex]: null;
    // TODO: null check for workout and workoutIndex
    return Scaffold(
      drawer: const NavigationDrawer(),
      appBar: AppBar(
        title: Text('Sets of workout: ' + workout!.title),
      ),
      body: Consumer<WorkoutsModel>(
        builder: (context, workoutsModel, _) => ListViewWithDivider(
          itemCount: workoutsModel.list[workoutIndex!].setElements.length,
          tileBuilder: (context, index) {
            final setElement = workoutsModel.list[workoutIndex].setElements[index];
            return SlidableTile(
              title: Consumer<SetTypesModel>(
                builder: (_, setTypesModel, __) {
                  return Text(setTypesModel.setTypeFromID(setElement.typeID)?.title ?? "No type");
                },
              ),
              subtitle: Consumer<ExercisesModel>(
                builder: (context, exercisesModel, child) {
                  return Text(exercisesModel.exerciseFromID(setElement.exerciseID)?.title ?? "No exercise");
                },
              ),
              onEdit: (context) {
                context.vRouter.to('edit/$index');
              },
              onDelete: (context) async {
                await EasyLoading.show(status: 'Deleting set...');
                await workoutsModel.deleteSet(
                  listIndex: index,
                  workoutListIndex: workoutIndex,
                );
                await EasyLoading.showSuccess('Finished');
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {context.vRouter.to('edit');},
        tooltip: 'Add a new set.',
        child: const Icon(Icons.add),
      ),
    );
  }
}