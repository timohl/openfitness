import 'package:flare_loading/flare_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:vrouter/vrouter.dart';

import 'package:pref/pref.dart';

class IntroView extends StatelessWidget {
  const IntroView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return IntroViewsFlutter(
      [
        PageViewModel(
          title: const Text('openfitness'),
          mainImage: FlareLoading(
            name: 'assets/trim.flr',
            startAnimation: 'start',
            onSuccess: (_) {},
            onError: (_, __) {},
          ),
          body: const Text('Welcome!'),
          pageColor: theme.scaffoldBackgroundColor,
          bubbleBackgroundColor: theme.backgroundColor,
          iconColor: theme.iconTheme.color,
          textStyle: theme.textTheme.bodyText2,
          titleTextStyle: theme.textTheme.headline6!,
          bodyTextStyle: theme.textTheme.bodyText1!,
        ),
      ],
      onTapDoneButton: () {
        PrefService.of(context).set('show_intro', false);
        context.vRouter.to('/home');
      },
      background: theme.scaffoldBackgroundColor,
      pageButtonTextStyles: theme.textTheme.button,
    );
  }
}
