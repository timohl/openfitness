import 'package:openfitness/model/execution_types_model.dart';
import 'package:openfitness/model/exercises_model.dart';
import 'package:openfitness/model/set_types_model.dart';
import 'package:openfitness/model/workouts_model.dart';

import 'package:flutter/material.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:provider/provider.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:timeline_tile/timeline_tile.dart';
import 'package:timer_builder/timer_builder.dart';
import 'package:vrouter/vrouter.dart';

class ExecuteWorkoutView extends StatefulWidget {
  const ExecuteWorkoutView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => ExecuteWorkoutViewState();
}

class ExecuteWorkoutViewState extends State<ExecuteWorkoutView> {
  int currentIndex = 0;
  final _setTimelineController = ItemScrollController();

  @override
  Widget build(BuildContext context) {
    final workoutIndexStr = (context.vRouter.pathParameters['workoutIndex'])!;
    final workoutIndex = int.parse(workoutIndexStr);
    return Consumer4<WorkoutsModel, ExercisesModel, SetTypesModel, ExecutionTypesModel>(
      builder: (context, workouts, exercises, setTypes, executionTypes, _) => Scaffold(
        body: Column(
          children: [
            // TODO: sizing of each area
            Expanded(
            // TODO: Move sets timeline to widgets
              child: ScrollablePositionedList.builder(
                initialAlignment: 0.5,
                itemScrollController: _setTimelineController,
                padding: const EdgeInsets.only(bottom: 5),
                scrollDirection: Axis.horizontal,
                itemBuilder:  (context, index) {
                  if(index == 0 || index == workouts.list[workoutIndex].setElements.length + 1) {
                    return const Padding(padding: EdgeInsets.symmetric(horizontal: 100));
                  }
                  final realIndex = index - 1;
                  final setElement = workouts.list[workoutIndex].setElements[realIndex];
                  final exercise = setElement.exerciseID != null? exercises.exerciseFromID(setElement.exerciseID!): null;
                  final executionType = executionTypes.executionTypeFromId(setElement.executionTypeID)!;
                  final setType = setTypes.setTypeFromID(setElement.typeID)!;
                  return TimelineTile(
                    indicatorStyle: IndicatorStyle(
                      color: realIndex < currentIndex? Colors.green: realIndex == currentIndex? Colors.yellow: Colors.red,
                    ),
                    alignment: TimelineAlign.end,
                    axis: TimelineAxis.horizontal,
                    isFirst: realIndex == 0,
                    isLast: realIndex == workouts.list[workoutIndex].setElements.length - 1,
                    startChild: Row(
                      children: [
                        if(realIndex != 0) const VerticalDivider(),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(exercise?.title ?? 'N/A'),
                            Text(setType.title),
                            Text(executionType.title),
                          ],
                        ),
                      ],
                    ),
                  );
                },
                itemCount: workouts.list[workoutIndex].setElements.length + 2,
              ),
            ),
            Row(
              children: workouts.list[workoutIndex].setElements.asMap().entries.map((indexSetPair) => Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 1.5, right: 1.5, top: 3),
                  child: GestureDetector(
                    onTap: () {
                      _scrollTo(indexSetPair.key);
                    },
                    child: Container(
                      color: indexSetPair.key < currentIndex? Colors.green: indexSetPair.key == currentIndex? Colors.yellow: Colors.red,
                      height: 10,
                    ),
                  ),
                ),
              )).toList(),
            ),
            // TODO: Video area
            const Expanded(child: Placeholder(), flex: 5),
            // TODO: Reps + timer area
            Expanded(
              child: Column(
                children: [
                  Expanded(child: 
                    Row(
                      children: [
                        Expanded(child: Container()),
                        const Expanded(
                          child: Center(
                            child: Text('Reps', textScaleFactor: 2,),
                          ),
                        ),
                        const Expanded(
                          child: Icon(Icons.info_outline, size: 42),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      children: [
                        Expanded(child: Container()),
                        Expanded(
                          child: Center(
                            child: TimerBuilder.periodic(
                              const Duration(seconds: 1),
                              builder: (context) => Text('${DateTime.now()}'),
                            ),
                          ),
                        ),
                        const Expanded(
                          child: Icon(Icons.pause, size: 42),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              flex: 4,
            ),
          ],
        ),
        bottomNavigationBar: ConvexAppBar(
          style: TabStyle.fixed,
          initialActiveIndex: 1,
          items: const [
            TabItem(icon: Icons.skip_previous, title: 'Back'),
            TabItem(icon: Icons.done, title: 'Done'),
            TabItem(icon: Icons.skip_next, title: 'Skip'),
          ],
          onTap: (index) {
            if(index == 0) {
              _back(workouts.list[workoutIndex].setElements);
            } else if(index == 1) {
              _done(workouts.list[workoutIndex].setElements);
            } else if(index == 2) {
              _skip(workouts.list[workoutIndex].setElements);
            }
          },
        ),
      ),
    );
  }
  
  void _scrollTo(int index) {
    _setTimelineController.scrollTo(
      index: index + 1, // Due to padding element at index 0
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeInOut,
      alignment: 0.4,
    );
  }
  
  void _moveTo(int index, List<SetElementData> setElements) {
    if(index >= 0 && index < setElements.length) {
      setState(() {
        currentIndex = index;
      });
      _scrollTo(index);
      _execute(index);
    }
  }

  void _skip(List<SetElementData> setElements) {
    // TODO: Add warning dialog
    _moveTo(currentIndex + 1, setElements);
  }
  
  void _done(List<SetElementData> setElements) {
    // TODO: Add dialog to enter reps
    _moveTo(currentIndex + 1, setElements);
  }
  
  
  void _back(List<SetElementData> setElements) {
    _moveTo(currentIndex - 1, setElements);
  }
  
  void _execute(int index) {
    // TODO: Reset timer
  }
}