import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:vrouter/vrouter.dart';

import 'package:openfitness/model/exercises_model.dart';
import 'package:openfitness/widget/navigation_drawer.dart';

class ShowExerciseView extends StatelessWidget {
  const ShowExerciseView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ExercisesModel>(
      builder: (context, ExercisesModel exercises, child) {
        final exerciseIndexStr = context.vRouter.pathParameters['exerciseIndex'];
        final exerciseIndex = exerciseIndexStr != null? int.parse(exerciseIndexStr): null;
        final exercise = exerciseIndex != null? exercises.list[exerciseIndex]: null;
        // TODO: exercise null check
        return Scaffold(
          drawer: const NavigationDrawer(),
          appBar: AppBar(
            title: Text(exercise!.title),
          ),
          body: Column(
            children: <Widget>[
              const Text('Description:'),
              Text(exercise.description ?? '<empty>'),
              const Text('Video:'),
              exercise.videoID == null? const Text('<empty>'): YoutubePlayer(
                controller: YoutubePlayerController(
                  initialVideoId: YoutubePlayer.convertUrlToId(exercise.videoID!) ?? '',
                  flags: const YoutubePlayerFlags(
                    autoPlay: false,
                  ),
                ),
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {context.vRouter.to('edit');},
            tooltip: 'Edit exercise.',
            child: const Icon(Icons.edit),
          ),
        );
      },
    );
  }
}