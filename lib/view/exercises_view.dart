import 'package:flutter/material.dart';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:vrouter/vrouter.dart';

import 'package:openfitness/model/exercises_model.dart';
import 'package:openfitness/widget/navigation_drawer.dart';
import 'package:openfitness/widget/listview_with_divider.dart';
import 'package:openfitness/widget/slidable_tile.dart';

class ExercisesView extends StatelessWidget {
  const ExercisesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavigationDrawer(),
      appBar: AppBar(
        title: const Text('Exercises'),
      ),
      body: Consumer<ExercisesModel>(
        builder: (context, exercisesModel, _) => ListViewWithDivider(
          itemCount: exercisesModel.list.length,
          tileBuilder: (context, index) => SlidableTile(
            title: Text(exercisesModel.list[index].title),
            onEdit: (context) {
              context.vRouter.to('edit/$index');
            },
            onTap: () {
              context.vRouter.to('show/$index');
            },
            onDelete: (context) async {
              await EasyLoading.show(status: 'Deleting exercise...');
              await Provider.of<ExercisesModel>(context, listen: false).delete(index);
              await EasyLoading.showSuccess('Finished');
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {context.vRouter.to('edit');},
        tooltip: 'Add a new exercise.',
        child: const Icon(Icons.add),
      ),
    );
  }
}