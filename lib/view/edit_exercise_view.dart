import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:vrouter/vrouter.dart';

import 'package:openfitness/model/exercises_model.dart';
import 'package:openfitness/widget/navigation_drawer.dart';

class EditExerciseView extends StatefulWidget {
  const EditExerciseView({Key? key}) : super(key: key);

  @override
  EditExerciseViewState createState() => EditExerciseViewState();
}

class EditExerciseViewState extends State<EditExerciseView> {
  final _formKey = GlobalKey<FormState>();
  String? _title;
  String? _description;
  String? _videoID;

  @override
  Widget build(BuildContext context) {
    final exerciseIndexStr = context.vRouter.pathParameters['exerciseIndex'];
    final exerciseIndex = exerciseIndexStr != null? int.parse(exerciseIndexStr): null;
    // TODO: Consumer or Provider.of?
    final exercise = exerciseIndex != null? Provider.of<ExercisesModel>(context, listen: false).list[exerciseIndex]: null;
    return Form(
      key: _formKey,
      child: Scaffold(
        drawer: const NavigationDrawer(),
        appBar: AppBar(
          title: Text(
            exercise != null? 'Edit Exercise': 'Add Exercise',
          ),
        ),
        body: Column(
          children: <Widget>[
            TextFormField(
              decoration: const InputDecoration(
                labelText: 'Name',
              ),
              validator: (value) => value?.isEmpty ?? false? 'Please enter a name.': null,
              onSaved: (value){_title = value;},
              initialValue: exercise?.title,
            ),
            TextFormField(
              decoration: const InputDecoration(
                labelText: 'Description',
              ),
              onSaved: (value){_description = value != ''? value: null;},
              initialValue: exercise?.description,
            ),
            TextFormField(
              decoration: const InputDecoration(
                labelText: 'Youtube Video Link/ID',
              ),
              onSaved: (value){_videoID = value != ''? value: null;},
              initialValue: exercise?.videoID,
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            if(_formKey.currentState!.validate()) {
              _formKey.currentState!.save();
              await EasyLoading.show(status: 'Saving changes...');
              final exerciseData = ExerciseData(
                title: _title!,
                description: _description,
                videoID: _videoID,
              );
              if(exerciseIndex != null) {
                await Provider.of<ExercisesModel>(context, listen: false).update(exerciseIndex, exerciseData);
              } else {
                await Provider.of<ExercisesModel>(context, listen: false).add(exerciseData);
              }
              await EasyLoading.showSuccess('Finished');
              context.vRouter.pop();
            }
          },
          tooltip: 'Done',
          child: const Icon(Icons.done),
        ),
      ),
    );
  }
}