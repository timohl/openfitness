import 'package:flutter/material.dart';

import 'package:vrouter/vrouter.dart';


class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<ListTile> navOptions = [];
    navOptions.add(ListTile(
      title: const Text('Home'),
      onTap: () {
        context.vRouter.to('/home');
      },
    ));
    navOptions.add(ListTile(
      title: const Text('Workouts'),
      onTap: () {
        context.vRouter.to('/workouts');
      },
    ));
    navOptions.add(ListTile(
      title: const Text('Exercises'),
      onTap: () {
        context.vRouter.to('/exercises');
      },
    ));
    ListTile settingsTile = ListTile(
      title: const Text('Settings'),
      onTap: () {
        context.vRouter.to('/settings');
      },
    );
    return Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const DrawerHeader(
            child: Text('openfitness'),
          ),
          Expanded(
            child: ListView(
              padding: EdgeInsets.zero,
              children: navOptions,
            ),
          ),
          settingsTile,
        ],
      ),
    );
  }
}
