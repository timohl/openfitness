import 'package:flutter/material.dart';

class ListViewWithDivider extends StatelessWidget {
  const ListViewWithDivider({
    Key? key,
    required this.itemCount,
    required this.tileBuilder,
  }) : super(key: key);
  
  final Widget Function(BuildContext context, int index) tileBuilder;
  final int itemCount;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemCount: itemCount * 2,
      itemBuilder: (context, i) {
        if(i.isOdd) return const Divider();
        int index = (i ~/ 2);
        return tileBuilder(context, index);
      },
    );
  }
}
