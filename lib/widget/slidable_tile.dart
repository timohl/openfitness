import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class SlidableTile extends StatelessWidget {
  const SlidableTile({
    Key? key,
    required this.title,
    this.subtitle,
    this.onStart,
    this.onEdit,
    this.onTap,
    this.onDelete,
  }) : super(key: key);

  final Widget title;
  final Widget? subtitle;
  final void Function(BuildContext context)? onStart;
  final void Function(BuildContext context)? onEdit;
  final void Function()? onTap;
  final void Function(BuildContext context)? onDelete;

  @override
  Widget build(BuildContext context) {
    return Slidable(
      startActionPane: ActionPane(
        motion: const BehindMotion(),
        children: [
          SlidableAction(
            label: 'Start',
            backgroundColor: Colors.green,
            icon: Icons.play_arrow,
            onPressed: onStart,
          ),
          SlidableAction(
            label: 'Edit',
            backgroundColor: Colors.blue,
            icon: Icons.edit,
            onPressed: onEdit,
          ),
        ],
      ),
      child: ListTile(
        title: title,
        subtitle: subtitle,
        leading: const CircleAvatar(
            backgroundColor: Colors.indigoAccent,
            foregroundColor: Colors.white,
        ),
        onTap: onTap,
      ),
      endActionPane: ActionPane(
        motion: const BehindMotion(),
        children: [
          SlidableAction(
            label: 'Delete',
            backgroundColor: Colors.red,
            icon: Icons.delete,
            onPressed: onDelete,
          ),
        ],
      ),
    );
  }


}