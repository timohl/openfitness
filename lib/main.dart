import 'package:flutter/material.dart';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:pref/pref.dart';
import 'package:dynamic_themes/dynamic_themes.dart';
import 'package:provider/provider.dart';
import 'package:vrouter/vrouter.dart';

import 'package:openfitness/db/database.dart';
import 'package:openfitness/model/exercises_model.dart';
import 'package:openfitness/model/workouts_model.dart';
import 'package:openfitness/model/set_types_model.dart';
import 'package:openfitness/model/execution_types_model.dart';
import 'package:openfitness/view/intro_view.dart';
import 'package:openfitness/view/home_view.dart';
import 'package:openfitness/view/settings_view.dart';
import 'package:openfitness/view/exercises_view.dart';
import 'package:openfitness/view/workouts_view.dart';
import 'package:openfitness/view/sets_view.dart';
import 'package:openfitness/view/edit_set_view.dart';
import 'package:openfitness/view/edit_exercise_view.dart';
import 'package:openfitness/view/edit_workout_view.dart';
import 'package:openfitness/view/execute_workout_view.dart';
import 'package:openfitness/view/show_exercise_view.dart';

void main() {
  initAndRun(const OpenfitnessApp());
}

void initAndRun(Widget widget) async {
  WidgetsFlutterBinding.ensureInitialized();
  final service = await PrefServiceShared.init(prefix: 'pref_');
  runApp(PrefService(
    service: service,
    child: widget,
  ));
}

class OpenfitnessApp extends StatelessWidget {
  const OpenfitnessApp({
    Key? key,
    this.initialUrl,
  }) : super(key: key);
  
  final String? initialUrl;

  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
      themeCollection: AppThemes.collection,
      defaultThemeId: AppThemes.dark,
      builder: (context, theme) {
        return MultiProvider(
          providers: [
            Provider(
              create: (context) => Database(),
              dispose: (context, Database db) => db.close(),
            ),
            ChangeNotifierProvider(
              create: (context) => ExercisesModel(
                Provider.of<Database>(context, listen: false),
              ),
            ),
            ChangeNotifierProvider(
              create: (context) => WorkoutsModel(
                Provider.of<Database>(context, listen: false),
              ),
            ),
            ChangeNotifierProvider(
              create: (context) => SetTypesModel(
                Provider.of<Database>(context, listen: false),
              ),
            ),
            ChangeNotifierProvider(
              create: (context) => ExecutionTypesModel(
                Provider.of<Database>(context, listen: false),
              ),
            ),
          ],
          builder: (context, _) => VRouter(
            title: 'openfitness',
            theme: theme,
            builder: EasyLoading.init(),
            debugShowCheckedModeBanner: false,
            initialUrl: initialUrl ?? '/home',
            beforeEnter: (_) async {
              // TODO: Add loading indicator
              await Provider.of<ExercisesModel>(context, listen: false).load();
              await Provider.of<WorkoutsModel>(context, listen: false).load();
              await Provider.of<SetTypesModel>(context, listen: false).load();
              await Provider.of<ExecutionTypesModel>(context, listen: false).load();
            },
            routes: [
              VWidget(path: '/intro', widget: const IntroView()),
              VGuard(
                beforeEnter: (vRedirector) async {
                  if(PrefService.of(context).get('show_intro') ?? true) {
                    return vRedirector.to('/intro');
                  }
                },
                stackedRoutes: [ // TODO: Check if parameters can be passed to view here instead of inside view
                  VWidget(path: '/home', widget: const HomeView()),
                  VWidget(path: '/settings', widget: const SettingsView()),
                  VWidget(path: '/workouts', widget: const WorkoutsView(), stackedRoutes: [
                    VWidget(path: 'edit', widget: const EditWorkoutView()),
                    VWidget(path: r'edit/:workoutIndex(\d+)', widget: const EditWorkoutView()),
                    VWidget(path: r'sets/:workoutIndex(\d+)', widget: const SetsView(), stackedRoutes: [
                      VWidget(path: 'edit', widget: const EditSetView()),
                      VWidget(path: r'edit/:setIndex(\d+)', widget: const EditSetView()),
                    ]),
                    VWidget(path: r'execute/:workoutIndex(\d+)', widget: const ExecuteWorkoutView()),
                  ]),
                  VWidget(path: '/exercises', widget: const ExercisesView(), stackedRoutes: [
                    VWidget(path: 'edit', widget: const EditExerciseView()),
                    VWidget(path: r'edit/:exerciseIndex(\d+)', widget: const EditExerciseView()),
                    VWidget(path: r'show/:exerciseIndex(\d+)', widget: const ShowExerciseView(), stackedRoutes: [
                      VWidget(path: 'edit', widget: const EditExerciseView()),
                    ]),
                  ]),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
