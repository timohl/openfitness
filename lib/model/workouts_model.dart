import 'dart:collection';

import 'package:flutter/foundation.dart';

import 'package:openfitness/db/database.dart';

class SetElementData {
  const SetElementData({
    this.exerciseID,
    required this.typeID,
    required this.executionTypeID,
    this.id,
    this.groupWithPrevious,
    this.minReps,
    this.maxReps,
    this.hold,
    this.lenght,
    this.rest,
    this.repRest,
  });

  final int? exerciseID;
  final int typeID;
  final int executionTypeID;
  final int? id;
  
  final bool? groupWithPrevious;
  final int? minReps;
  final int? maxReps;
  final int? hold;
  final int? lenght;
  final int? rest;
  final int? repRest;
}

class WorkoutData {
  WorkoutData({required this.title, this.description, this.id, List<SetElementData>? setElements}) :
  setElements = setElements ?? [];

  final String title;
  final String? description;
  final List<SetElementData> setElements;
  final int? id;
}

class WorkoutsModel extends ChangeNotifier {
  WorkoutsModel(this.db);

  final Database db;
  final List<WorkoutData> _workouts = [];

  UnmodifiableListView<WorkoutData> get list => UnmodifiableListView(_workouts);

  Future<void> load() async {
    // Clear workouts list and get fresh version
    _workouts.clear();
    _workouts.addAll((await db.workoutsDao.allEntries).map((entry) => WorkoutData(
      title: entry.title,
      description: entry.description,
      id: entry.id,
    )));
    // Add sets to workouts
    final setElements = await db.setElementsDao.allEntries;
    for(final setElement in setElements) {
      workoutFromID(setElement.workoutID).setElements.add(SetElementData(
        exerciseID: setElement.exerciseID,
        typeID: setElement.typeID,
        executionTypeID: setElement.executionTypeID,
        id: setElement.id,
      ));
    }
    notifyListeners();
  }

  Future<void> add(WorkoutData workout) async {
    await db.workoutsDao.addEntry(
      _workouts.length,
      workout.title,
      workout.description,
    );
    _workouts.add(workout);
    notifyListeners();
  }

  Future<void> update(WorkoutData workout, int listIndex) async {
    await db.workoutsDao.updateEntry(
      listIndex,
      workout.title,
      workout.description,
    );
    _workouts[listIndex] = workout;
    notifyListeners();
  }

  Future<void> delete(int listIndex) async {
    await db.workoutsDao.deleteWorkoutAndSetsAndUpdateIndex(listIndex);
    _workouts.removeAt(listIndex);
    notifyListeners();
  }

  Future<void> deleteSet({
    required int listIndex,
    required int workoutListIndex,
  }) async {
    final workoutID = await db.workoutsDao.getID(workoutListIndex);
    await db.setElementsDao.deleteEntryAndUpdateIndex(
      listIndex: listIndex,
      workoutID: workoutID,
    );
    _workouts[workoutListIndex].setElements.removeAt(listIndex);
    notifyListeners();
  }

  Future<void> addSet({
    required SetElementData setElement,
    required int workoutListIndex,
  }) async {
    final workoutID = await db.workoutsDao.getID(workoutListIndex);
    await db.setElementsDao.addEntry(
      listIndex: _workouts[workoutListIndex].setElements.length,
      workoutID: workoutID,
      exerciseID: setElement.exerciseID,
      typeID: setElement.typeID,
      executionTypeID: setElement.executionTypeID,
      groupWithPrevious: setElement.groupWithPrevious,
      minReps: setElement.minReps,
      maxReps: setElement.maxReps,
      hold: setElement.hold,
      lenght: setElement.lenght,
      rest: setElement.rest,
      repRest: setElement.repRest,
    );
    _workouts[workoutListIndex].setElements.add(setElement);
    notifyListeners();
  }

  Future<void> updateSet({
    required SetElementData setElement,
    required int listIndex,
    required int workoutListIndex,
  }) async {
    final workoutID = await db.workoutsDao.getID(workoutListIndex);
    await db.setElementsDao.updateEntry(
      listIndex: listIndex,
      workoutID: workoutID,
      exerciseID: setElement.exerciseID,
      typeID: setElement.typeID,
      executionTypeID: setElement.executionTypeID,
      groupWithPrevious: setElement.groupWithPrevious,
      minReps: setElement.minReps,
      maxReps: setElement.maxReps,
      hold: setElement.hold,
      lenght: setElement.lenght,
      rest: setElement.rest,
      repRest: setElement.repRest,
    );
    _workouts[workoutListIndex].setElements[listIndex] = setElement;
    notifyListeners();
  }

  int _listIndexFromID(int id) {
    for(int listIndex = 0; listIndex != list.length; ++listIndex) {
      if(_workouts[listIndex].id == id) {
        return listIndex;
      }
    }
    throw Exception('Cannot find ID in workout list.');
  }

  WorkoutData workoutFromID(int id) {
    return _workouts[_listIndexFromID(id)];
  }
}