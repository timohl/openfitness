import 'dart:collection';

import 'package:openfitness/model/execution_types_model.dart';
import 'package:openfitness/model/set_types_model.dart';
import 'package:openfitness/model/workouts_model.dart';

class GatedAttribute<T> {
  const GatedAttribute({
    required T? value,
    required this.enabled,
  }) :
    _value = value;

  final T? _value;
  final bool enabled;

  T? get value => enabled? _value: null;
}

class GatedSetElement {
  GatedSetElement({
    SetElementData? setElement,
    SetTypeData? setType,
    ExecutionTypeData? executionType,
  }) :
    _setElement = setElement,
    _enabledAttributes = HashSet.from(
      (setType?.enabledAttributes ?? HashSet<String>()).union(executionType?.enabledAttributes ?? HashSet<String>()),
    );
  
  final SetElementData? _setElement;
  final HashSet<String> _enabledAttributes;
  
  GatedAttribute<T> returnGatedAttribute<T>(T? t, String name) => GatedAttribute(
    enabled: _enabledAttributes.contains(name),
    value: t,
  );
  
  GatedAttribute<bool> get groupWithPrevious => returnGatedAttribute(
    _setElement?.groupWithPrevious,
    'groupWithPrevious',
  );

  GatedAttribute<int> get minReps => returnGatedAttribute(
    _setElement?.minReps,
    'minReps',
  );

  GatedAttribute<int> get maxReps => returnGatedAttribute(
    _setElement?.maxReps,
    'maxReps',
  );

  GatedAttribute<int> get hold => returnGatedAttribute(
    _setElement?.hold,
    'hold',
  );

  GatedAttribute<int> get lenght => returnGatedAttribute(
    _setElement?.lenght,
    'lenght',
  );

  GatedAttribute<int> get rest => returnGatedAttribute(
    _setElement?.rest,
    'rest',
  );

  GatedAttribute<int> get repRest => returnGatedAttribute(
    _setElement?.repRest,
    'repRest',
  );
}