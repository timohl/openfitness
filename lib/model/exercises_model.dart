import 'dart:collection';

import 'package:flutter/foundation.dart';

import 'package:openfitness/db/database.dart';

class ExerciseData {
  const ExerciseData({required this.title, required this.description, required this.videoID, this.id});

  final String title;
  final String? description;
  final String? videoID;
  final int? id;
}

class ExercisesModel extends ChangeNotifier {
  ExercisesModel(this.db);

  final Database db;
  final List<ExerciseData> _exercises = [];

  UnmodifiableListView<ExerciseData> get list => UnmodifiableListView(_exercises);

  Future<void> load() async {
    _exercises.clear();
    _exercises.addAll((await db.exercisesDao.allEntries).map((entry) => ExerciseData(
      title: entry.title,
      description: entry.description, 
      videoID: entry.videoID,
      id: entry.id,
    )));
    notifyListeners();
  }

  Future<void> add(ExerciseData exercise) async {
    await db.exercisesDao.addEntry(
      _exercises.length,
      exercise.title,
      exercise.description,
      exercise.videoID,
    );
    _exercises.add(exercise);
    notifyListeners();
  }

  Future<void> update(int listIndex, ExerciseData exercise) async {
    await db.exercisesDao.updateEntry(
      listIndex,
      exercise.title,
      exercise.description,
      exercise.videoID,
    );
    _exercises[listIndex] = exercise;
    notifyListeners();
  }

  Future<void> delete(int listIndex) async {
    final exerciseID = await db.exercisesDao.getExerciseID(listIndex);
    await db.setElementsDao.removeExerciseReferences(exerciseID);
    await db.exercisesDao.deleteEntryAndUpdateIndex(listIndex);
    _exercises.removeAt(listIndex);
    notifyListeners();
  }

  int? listIndexFromID(int? id) {
    for(int listIndex = 0; listIndex != list.length; ++listIndex) {
      if(_exercises[listIndex].id == id) {
        return listIndex;
      }
    }
    return null;
  }

  ExerciseData? exerciseFromID(int? id) {
    final listIndex = listIndexFromID(id);
    return listIndex != null? _exercises[listIndex]: null;
  }
}