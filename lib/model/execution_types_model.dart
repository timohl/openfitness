import 'dart:collection';

import 'package:flutter/foundation.dart';

import 'package:openfitness/db/database.dart';

class ExecutionTypeData {
  const ExecutionTypeData({
    required this.title,
    this.description,
    this.id,
    required this.enabledAttributes,
  });

  final String title;
  final String? description;
  final int? id;
  final HashSet<String> enabledAttributes;
}

class ExecutionTypesModel extends ChangeNotifier {
  ExecutionTypesModel(this.db);

  final Database db;
  final List<ExecutionTypeData> _executionTypes = [];

  UnmodifiableListView<ExecutionTypeData> get list => UnmodifiableListView(_executionTypes);

  Future<void> load() async {
    // Clear rep types list and get fresh version
    _executionTypes.clear();
    _executionTypes.addAll(
      (await db.executionTypesDao.allEntries).map(
        (entry) => ExecutionTypeData(
          title: entry.title,
          description: entry.description,
          id: entry.id,
          enabledAttributes: entry.enabledAttributes ?? HashSet<String>(),
        ),
      ),
    );
    if(_executionTypes.isEmpty) {
      await _addDefaultExecutionTypes();
    }
  }

  Future<void> _addDefaultExecutionTypes() async {
    await _addExecutionType(ExecutionTypeData(
      title: "Hold",
      description: "Hold exercise for a certain time.",
      enabledAttributes: HashSet.from({
        'hold',
      }),
    ));
    await _addExecutionType(ExecutionTypeData(
      title: "Repeat",
      description: "Repeat exercise for a certain number of repetitions.",
      enabledAttributes: HashSet.from({
        'minReps',
        'maxReps',
      }),
    ));
    await _addExecutionType(ExecutionTypeData(
      title: "Interval",
      description: "Repeat exercise for a certain time.",
      enabledAttributes: HashSet.from({
        'lenght',
      }),
    ));
    await _addExecutionType(ExecutionTypeData(
      title: "Repeat until failure",
      description: "Repeat exercise until you cannot do anymore repetitions.",
      enabledAttributes: HashSet.from({}),
    ));
    await _addExecutionType(ExecutionTypeData(
      title: "Hold until failure",
      description: "Hold exercise until you cannot hold anymore.",
      enabledAttributes: HashSet.from({
        'hold',
      }),
    ));
  }

  Future<void> _addExecutionType(ExecutionTypeData executionType) async {
    await db.executionTypesDao.addEntry(
      title: executionType.title,
      description: executionType.description,
      enabledAttributes: executionType.enabledAttributes,
    );
    _executionTypes.add(executionType);
  }

  ExecutionTypeData? executionTypeFromId(int? id) {
    if(id == null) return null;
    for(final executionType in _executionTypes) {
      if(executionType.id == id) {
        return executionType;
      }
    }
    return null;
  }
}