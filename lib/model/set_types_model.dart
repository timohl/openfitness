import 'dart:collection';

import 'package:flutter/foundation.dart';

import 'package:openfitness/db/database.dart';

class SetTypeData {
  const SetTypeData({
    required this.title,
    this.description,
    this.id,
    required this.enabledAttributes,
  });

  final String title;
  final String? description;
  final int? id;
  final HashSet<String> enabledAttributes;
}

class SetTypesModel extends ChangeNotifier {
  SetTypesModel(this.db);

  final Database db;
  final List<SetTypeData> _setTypes = [];

  UnmodifiableListView<SetTypeData> get list => UnmodifiableListView(_setTypes);

  Future<void> load() async {
    // Clear set types list and get fresh version
    _setTypes.clear();
    _setTypes.addAll((await db.setTypesDao.allEntries).map((entry) => SetTypeData(
      title: entry.title,
      description: entry.description,
      id: entry.id,
      enabledAttributes: entry.enabledAttributes ?? HashSet<String>(),
    )));
    if(_setTypes.isEmpty) {
      await _addDefaultSetTypes();
    }
    notifyListeners();
  }

  Future<void> _addDefaultSetTypes() async {
    await _addSetType(SetTypeData(
      title: "Interval Set",
      description: "Perform n reps within time t and rest for the remaining duration of t.",
      enabledAttributes: HashSet.from({
        'minReps',
        'maxReps',
        'lenght',
      }),
    ));
    await _addSetType(SetTypeData(
      title: "Super Set",
      description: "Hard exercise with low reps paired with easier higher rep exercise.",
      enabledAttributes: HashSet.from({
        'groupWithPrevious',
      }),
    ));
    await _addSetType(SetTypeData(
      title: "Ladders",
      description: "For a time interval do n reps then rest for n secs, continue with n+1 until you are about to fail. Then continue with n-1.",
      enabledAttributes: HashSet.from({
        'lenght',
      }),
    ));
    await _addSetType(SetTypeData(
      title: "HIIT",
      description: "High Intensity Interval Training.",
      enabledAttributes: HashSet.from({
        'minReps',
        'maxReps',
        'repRest',
      }),
    ));
    await _addSetType(SetTypeData(
      title: "Tabata",
      description: "8x(20sec work, 10sec rest).",
      enabledAttributes: HashSet.from({
        'minReps',
        'maxReps',
      }),
    ));
    await _addSetType(SetTypeData(
      title: "Timed",
      description: "Do the exercise during the time interval.",
      enabledAttributes: HashSet.from({
        'lenght',
      }),
    ));
  }

  Future<void> _addSetType(SetTypeData setType) async {
    await db.setTypesDao.addEntry(
      title: setType.title,
      description: setType.description,
      enabledAttributes: setType.enabledAttributes,
    );
    _setTypes.add(setType);
  }

  SetTypeData? setTypeFromID(int? id) {
    if(id == null) return null;
    for(var setType in _setTypes) {
      if(setType.id == id) {
        return setType;
      }
    }
    return null;
  }

}