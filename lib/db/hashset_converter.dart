import 'dart:collection';
import 'dart:convert';

import 'package:drift/drift.dart';

class HashSetConverter extends NullAwareTypeConverter<HashSet<String>, String> {
  @override
  HashSet<String> requireMapToDart(String fromDb) => HashSet<String>.from((json.decode(fromDb) as List<dynamic>).cast<String>());

  @override
  String requireMapToSql(HashSet<String> value) => json.encode(value.toList());
}