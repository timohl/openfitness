import 'package:drift/drift.dart';

import 'package:openfitness/db/database.dart';
import 'package:openfitness/db/tables.dart';

part 'workouts_dao.g.dart';

@DriftAccessor(tables: [Workouts])
class WorkoutsDao extends DatabaseAccessor<Database> with _$WorkoutsDaoMixin {
  WorkoutsDao(Database db) : super(db);
  
  Future<List<Workout>> get allEntries => (
    select(workouts)
    ..orderBy([(w) => OrderingTerm(expression: w.listIndex)])
  ).get();
  
  Future addEntry(
    int listIndex,
    String title,
    String? description,
  ) =>  into(workouts).insert(WorkoutsCompanion(
    listIndex: Value(listIndex),
    title: Value(title),
    description: Value(description),
  ));

  Future updateEntry(
    int listIndex,
    String title,
    String? description,
  ) => (
    update(workouts)
    ..where((w) => w.listIndex.equals(listIndex))
  ).write(WorkoutsCompanion(
    title: Value(title),
    description: Value(description),
  ));

  Future _deleteEntry(int listIndex) => (
    delete(workouts)
    ..where((e) => e.listIndex.equals(listIndex))
  ).go();

  Future _updateListIndexAfter(int listIndex) => (
    update(workouts)
    ..where((e) => e.listIndex.isBiggerThanValue(listIndex))
  ).write(WorkoutsCompanion.custom(
    listIndex: (workouts.listIndex - const Constant(1)).dartCast<int>(),
  ));
  
  Future _deleteEntryAndUpdateIndex(int listIndex) => transaction(() async {
    await _deleteEntry(listIndex);
    await _updateListIndexAfter(listIndex);
  });
  
  Future<int> getID(int listIndex) => (
    select(workouts)
    ..where((w) => w.listIndex.equals(listIndex))
  ).map((w) => w.id).getSingle();
  
  Future deleteWorkoutAndSetsAndUpdateIndex(int listIndex) => transaction(() async {
    await _deleteEntryAndUpdateIndex(listIndex);
    await db.setElementsDao.deleteEntriesOfWorkout(await getID(listIndex));
  });
}