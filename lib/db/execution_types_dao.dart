import 'dart:collection';

import 'package:drift/drift.dart';

import 'package:openfitness/db/database.dart';
import 'package:openfitness/db/tables.dart';

part 'execution_types_dao.g.dart';

@DriftAccessor(tables: [ExecutionTypes])
class ExecutionTypesDao extends DatabaseAccessor<Database> with _$ExecutionTypesDaoMixin {
  ExecutionTypesDao(Database db) : super(db);
  
  Future<List<ExecutionType>> get allEntries => select(executionTypes).get();

  Future addEntry({
    required String title,
    String? description,
    HashSet<String>? enabledAttributes,
  }) => into(executionTypes).insert(ExecutionTypesCompanion(
    title: Value(title),
    description: Value(description),
    enabledAttributes: Value(enabledAttributes),
  ));
}