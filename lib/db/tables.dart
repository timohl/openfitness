import 'package:drift/drift.dart';
import 'package:openfitness/db/hashset_converter.dart';

class Exercises extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get listIndex => integer()();
  TextColumn get title => text()();
  TextColumn get description => text().nullable()();
  TextColumn get videoID => text().nullable()();
}

class Workouts extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get listIndex => integer()();
  TextColumn get title => text()();
  TextColumn get description => text().nullable()();
}

class SetTypes extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get title => text()();
  TextColumn get description => text().nullable()();
  TextColumn get enabledAttributes => text().map(HashSetConverter()).nullable()();
}

class ExecutionTypes extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get title => text()();
  TextColumn get description => text().nullable()();
  TextColumn get enabledAttributes => text().map(HashSetConverter()).nullable()();
}

class SetElements extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get listIndex => integer()();
  IntColumn get workoutID => integer()();
  IntColumn get exerciseID => integer().nullable()();
  IntColumn get typeID => integer()();
  BoolColumn get groupWithPrevious => boolean().nullable()();
  IntColumn get executionTypeID => integer()();
  IntColumn get minReps => integer().nullable()();
  IntColumn get maxReps => integer().nullable()();
  IntColumn get hold => integer().nullable()();
  IntColumn get lenght => integer().nullable()();
  IntColumn get rest => integer().nullable()();
  IntColumn get repRest => integer().nullable()();
}