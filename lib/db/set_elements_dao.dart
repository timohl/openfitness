import 'package:drift/drift.dart';

import 'package:openfitness/db/database.dart';
import 'package:openfitness/db/tables.dart';

part 'set_elements_dao.g.dart';

@DriftAccessor(tables: [SetElements])
class SetElementsDao extends DatabaseAccessor<Database> with _$SetElementsDaoMixin {
  SetElementsDao(Database db) : super(db);
  
  Future<List<SetElement>> get allEntries => (
    select(setElements)
    ..orderBy([(se) => OrderingTerm(expression: se.listIndex)])
  ).get();

  Future addEntry({
    required int listIndex,
    required int workoutID,
    int? exerciseID,
    required int typeID,
    required int executionTypeID,
    bool? groupWithPrevious,
    int? minReps,
    int? maxReps,
    int? hold,
    int? lenght,
    int? rest,
    int? repRest,
  }) => into(setElements).insert(SetElementsCompanion(
    listIndex: Value(listIndex),
    workoutID: Value(workoutID),
    exerciseID: Value(exerciseID),
    typeID: Value(typeID),
    executionTypeID: Value(executionTypeID),
    groupWithPrevious: Value(groupWithPrevious),
    minReps: Value(minReps),
    maxReps: Value(maxReps),
    hold: Value(hold),
    lenght: Value(lenght),
    rest: Value(rest),
    repRest: Value(repRest),
  ));
  
  Future updateEntry({
    required int listIndex,
    required int workoutID,
    int? exerciseID,
    required int typeID,
    required int executionTypeID,
    bool? groupWithPrevious,
    int? minReps,
    int? maxReps,
    int? hold,
    int? lenght,
    int? rest,
    int? repRest,
  }) => (
    update(setElements)
    ..where((se) => se.listIndex.equals(listIndex) & se.workoutID.equals(workoutID))
  ).write(SetElementsCompanion(
    exerciseID: Value(exerciseID),
    typeID: Value(typeID),
    executionTypeID: Value(executionTypeID),
    groupWithPrevious: Value(groupWithPrevious),
    minReps: Value(minReps),
    maxReps: Value(maxReps),
    hold: Value(hold),
    lenght: Value(lenght),
    rest: Value(rest),
    repRest: Value(repRest),
  ));

  Future _deleteEntry({
    required int listIndex,
    required int workoutID,
  }) => (
    delete(setElements)
    ..where((se) => se.listIndex.equals(listIndex) & se.workoutID.equals(workoutID))
  ).go();

  Future _updateListIndexAfter({
    required int listIndex,
    required int workoutID,
  }) => (
    update(setElements)
    ..where((se) => se.listIndex.isBiggerThanValue(listIndex) & se.workoutID.equals(workoutID))
  ).write(SetElementsCompanion.custom(
    listIndex: (setElements.listIndex - const Constant(1)).dartCast<int>(),
  ));
  
  Future deleteEntryAndUpdateIndex({
    required int listIndex,
    required int workoutID,
  }) => transaction(() async {
    await _deleteEntry(
      listIndex: listIndex,
      workoutID: workoutID,
    );
    await _updateListIndexAfter(
      listIndex: listIndex,
      workoutID: workoutID,
    );
  });
  
  Future deleteEntriesOfWorkout(int workoutID) => (
    delete(setElements)
    ..where((se) => se.workoutID.equals(workoutID))
  ).go();

  Future removeExerciseReferences(int exerciseID) => (
    update(setElements)
    ..where((se) => se.exerciseID.equals(exerciseID))
  ).write(const SetElementsCompanion(
    exerciseID: Value(null),
  ));
}