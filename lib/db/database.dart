import 'dart:io';
import 'dart:collection';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;

import 'package:openfitness/db/tables.dart';
import 'package:openfitness/db/exercises_dao.dart';
import 'package:openfitness/db/workouts_dao.dart';
import 'package:openfitness/db/execution_types_dao.dart';
import 'package:openfitness/db/set_elements_dao.dart';
import 'package:openfitness/db/set_types_dao.dart';
import 'package:openfitness/db/hashset_converter.dart';

part 'database.g.dart';

LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'db.sqlite'));
    return NativeDatabase(file);
  });
}

@DriftDatabase(
  tables: [
    Exercises, 
    Workouts,
    SetTypes,
    ExecutionTypes,
    SetElements,
  ],
  daos: [
    ExercisesDao,
    WorkoutsDao,
    SetElementsDao,
    SetTypesDao,
    ExecutionTypesDao,
  ],
)
class Database extends _$Database {
  Database({QueryExecutor? queryExecutor}) : super(queryExecutor ?? _openConnection());
  
  @override
  int get schemaVersion => 1;
}