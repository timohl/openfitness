import 'dart:collection';

import 'package:drift/drift.dart';

import 'package:openfitness/db/database.dart';
import 'package:openfitness/db/tables.dart';

part 'set_types_dao.g.dart';

@DriftAccessor(tables: [SetTypes])
class SetTypesDao extends DatabaseAccessor<Database> with _$SetTypesDaoMixin {
  SetTypesDao(Database db) : super(db);
  
  Future<List<SetType>> get allEntries => select(setTypes).get();

  Future<int> addEntry({
    required String title,
    String? description,
    HashSet<String>? enabledAttributes,
  }) =>  into(setTypes).insert(SetTypesCompanion(
    title: Value(title),
    description: Value(description),
    enabledAttributes: Value(enabledAttributes),
  ));
}