import 'package:drift/drift.dart';

import 'package:openfitness/db/database.dart';
import 'package:openfitness/db/tables.dart';

part 'exercises_dao.g.dart';

@DriftAccessor(tables: [Exercises])
class ExercisesDao extends DatabaseAccessor<Database> with _$ExercisesDaoMixin {
  ExercisesDao(Database db) : super(db);
  
  Future<List<Exercise>> get allEntries => (
    select(exercises)
    ..orderBy([(e) => OrderingTerm(expression: e.listIndex)])
  ).get();
  
  Future<int> addEntry(
    int listIndex,
    String title,
    String? description,
    String? videoID,
  ) => into(exercises).insert(ExercisesCompanion(
    listIndex: Value(listIndex),
    title: Value(title),
    description: Value(description),
    videoID: Value(videoID),
  ));
  
  Future updateEntry(int listIndex, String title, String? description, String? videoID) => (
    update(exercises)
    ..where((e) => e.listIndex.equals(listIndex))
  ).write(ExercisesCompanion(
    title: Value(title),
    description: Value(description),
    videoID: Value(videoID),
  ));

  Future _deleteEntry(int listIndex) => (
    delete(exercises)
    ..where((e) => e.listIndex.equals(listIndex))
  ).go();
  
  Future _updateListIndexAfter(int listIndex) => (
    update(exercises)
    ..where((e) => e.listIndex.isBiggerThanValue(listIndex))
  ).write(ExercisesCompanion.custom(
    listIndex: (exercises.listIndex - const Constant(1)).dartCast<int>(),
  ));
  
  Future<int> getExerciseID(int listIndex) => (
    select(exercises)
    ..where((e) => e.listIndex.equals(listIndex))
  ).map((e) => e.id).getSingle();
  
  Future deleteEntryAndUpdateIndex(int listIndex) => transaction(() async {
    await _deleteEntry(listIndex);
    await _updateListIndexAfter(listIndex);
  });
}