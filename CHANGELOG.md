## 0.1.1 - 2021-11-30
### Added
- CI pipeline for upload and release
- Automatic version bump pipeline searching for \[major\]/\[minor\] in git history

## 0.1.0 - 2021-11-25
### Added
- Versioning using cider
- Changelog using cider
- Check for changed CHANGELOG.md in merge requests