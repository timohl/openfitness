import 'package:drift/native.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:openfitness/db/database.dart';

void main() {
  Database? db;
  setUp(() {
    db = Database(queryExecutor: NativeDatabase.memory());
  });
  tearDown(() async {
    await db?.close();
  });
  
  test('DB should contain an entry after adding an entry', () async {
    const title = 'title';
    const description = 'description';
    final id = await db!.setTypesDao.addEntry(
      title: title,
      description: description,
    );
    final allEntries = await db!.setTypesDao.allEntries;
    expect(allEntries.length, 1);
    expect(allEntries[0].id, id);
    expect(allEntries[0].title, title);
    expect(allEntries[0].description, description);
  });

  test('DB should contain no entry after setUp', () async {
    final allEntries = await db!.setTypesDao.allEntries;
    expect(allEntries.length, 0);
  });
  
  test('Entry description should be null after adding entry without description', () async {
    await db!.setTypesDao.addEntry(title: 'title');
    final allEntries = await db!.setTypesDao.allEntries;
    expect(allEntries.length, 1);
    expect(allEntries[0].description, null);
  });
}