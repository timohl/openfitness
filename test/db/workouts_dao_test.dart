import 'package:drift/native.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:openfitness/db/database.dart';

// TODO: Add tests
void main() {
  Database? db;
  setUp(() {
    db = Database(queryExecutor: NativeDatabase.memory());
  });
  tearDown(() async {
    await db?.close();
  });
}  