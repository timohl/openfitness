import 'dart:collection';

import 'package:flutter_test/flutter_test.dart';
import 'package:openfitness/model/execution_types_model.dart';
import 'package:openfitness/model/gated_set_element.dart';
import 'package:openfitness/model/set_types_model.dart';
import 'package:openfitness/model/workouts_model.dart';

void main() {
  SetElementData? setElement;
  ExecutionTypeData? emptyExecutionType;
  SetTypeData? emptySetType;
  setUp(() {
    setElement = const SetElementData(
      exerciseID: 0,
      typeID: 1,
      executionTypeID: 2,
      id: 3,
      groupWithPrevious: true,
      minReps: 4,
      maxReps: 5,
      hold: 6,
      lenght: 7,
      rest: 8,
      repRest: 9,
    );
    emptyExecutionType = ExecutionTypeData(
      title: '',
      enabledAttributes: HashSet<String>(),
    );
    emptySetType = SetTypeData(
      title: '',
      enabledAttributes: HashSet<String>(),
    );
  });
  
  void testGated({
    required String varName,
    required dynamic Function() getFromSet,
    required dynamic Function(GatedSetElement) getFromGatedSet,
  }) {
    group(varName, () {
      ExecutionTypeData? executionType;
      SetTypeData? setType;
      setUp(() {
        executionType = ExecutionTypeData(
          title: '',
          enabledAttributes: HashSet<String>.from({varName}),
        );
        setType = SetTypeData(
          title: '',
          enabledAttributes: HashSet<String>.from({varName}),
        );
      });
      void testCombination(
        SetTypeData setType,
        bool setTypeContains,
        ExecutionTypeData executionType,
        bool executionTypeContains,
      ) {
        final gatedSetElement = GatedSetElement(
          setElement: setElement!,
          setType: setType,
          executionType: executionType,
        );
        expect(setType.enabledAttributes.contains(varName), setTypeContains);
        expect(executionType.enabledAttributes.contains(varName), executionTypeContains);
        expect(
          getFromGatedSet(gatedSetElement),
          setTypeContains || executionTypeContains? getFromSet(): null,
        );
      }
      test('should return its value if only enabled in setType', () {
        testCombination(setType!, true, emptyExecutionType!, false);
      });
      test('should return its value if only enabled in executionType', () {
        testCombination(emptySetType!, false, executionType!, true);
      });
      test('should return its value if enabled in setType and executionType', () {
        testCombination(setType!, true, executionType!, true);
      });
      test('should return null if not enabled', () {
        testCombination(emptySetType!, false, emptyExecutionType!, false);
      });
    });
  }
  
  testGated(
    varName: 'groupWithPrevious',
    getFromSet: () => setElement!.groupWithPrevious,
    getFromGatedSet: (GatedSetElement gse) => gse.groupWithPrevious.value,
  );
  testGated(
    varName: 'minReps',
    getFromSet: () => setElement!.minReps,
    getFromGatedSet: (GatedSetElement gse) => gse.minReps.value,
  );
  testGated(
    varName: 'maxReps',
    getFromSet: () => setElement!.maxReps,
    getFromGatedSet: (GatedSetElement gse) => gse.maxReps.value,
  );
  testGated(
    varName: 'hold',
    getFromSet: () => setElement!.hold,
    getFromGatedSet: (GatedSetElement gse) => gse.hold.value,
  );
  testGated(
    varName: 'lenght',
    getFromSet: () => setElement!.lenght,
    getFromGatedSet: (GatedSetElement gse) => gse.lenght.value,
  );
  testGated(
    varName: 'rest',
    getFromSet: () => setElement!.rest,
    getFromGatedSet: (GatedSetElement gse) => gse.rest.value,
  );
  testGated(
    varName: 'repRest',
    getFromSet: () => setElement!.repRest,
    getFromGatedSet: (GatedSetElement gse) => gse.repRest.value,
  );
}